# Монады в JavaScript

# Общее описание

# Монада

Монады - это концепция из функционального программирования, которая помогает управлять побочными эффектами и упрощает работу с асинхронным кодом. В JavaScript монады можно использовать для обработки ошибок, управления состоянием и других задач.

Promise в JavaScript является примером монады. Promise представляет собой объект, который возвращает значение, которое может быть доступно сейчас, в будущем или никогда.

Вот пример использования Promise как монады:

// Создаем Promise
let promise = new Promise((resolve, reject) => {
    setTimeout(() => resolve("Hello, World!"), 1000);
});

// Используем метод then для преобразования результата
promise.then(result => result.toUpperCase())
       .then(upperResult => console.log(upperResult)); // "HELLO, WORLD!"

// Метод catch для обработки ошибок
promise.catch(error => console.error(error));

В этом примере promise является монадой. Метод then позволяет применить функцию к результату Promise, когда он станет доступен. Метод catch позволяет обработать ошибки, которые могут возникнуть в процессе выполнения Promise.

# Монада Maybe

Монада Maybe в JavaScript используется для обработки возможных отсутствующих значений, без необходимости проверки на null или undefined. Вот пример реализации и использования монады Maybe:

class Maybe {
    constructor(value) {
        this._value = value;
    }

    static just(value) {
        return new Just(value);
    }

    static nothing() {
        return new Nothing();
    }

    static fromNullable(value) {
        return value != null ? Maybe.just(value) : Maybe.nothing();
    }

    getOrElse() {
        return this._value;
    }
}

class Just extends Maybe {
    map(fn) {
        return Maybe.fromNullable(fn(this._value));
    }

    chain(fn) {
        return fn(this._value);
    }

    toString() {
        return `Maybe.Just(${this._value})`;
    }
}

class Nothing extends Maybe {
    map(fn) {
        return this;
    }

    chain(fn) {
        return this;
    }

    getOrElse(other) {
        return other;
    }

    toString() {
        return 'Maybe.Nothing';
    }
}

// Пример использования
const maybe1 = Maybe.fromNullable('Hello World').map(s => s.toUpperCase());
console.log(maybe1.toString()); // Maybe.Just(HELLO WORLD)

const maybe2 = Maybe.fromNullable(null).map(s => s.toUpperCase());
console.log(maybe2.toString()); // Maybe.Nothing

В этом примере Maybe - это монада, которая может содержать значение (Just) или не содержать его (Nothing). Метод map позволяет применить функцию к содержимому монады, если оно есть. Если монада пуста (Nothing), то map просто возвращает пустую монаду.

# Монада Exception

В JavaScript нет встроенной монады Exception, но мы можем создать её самостоятельно. Монада Exception позволяет обрабатывать ошибки в функциональном стиле. Вот пример реализации и использования монады Exception:

class Exception {
    constructor(value) {
        this._value = value;
    }

    static of(value) {
        if (value instanceof Error) {
            return new Failure(value);
        } else {
            return new Success(value);
        }
    }

    map(fn) {
        return this;
    }
}

class Success extends Exception {
    map(fn) {
        try {
            return Exception.of(fn(this._value));
        } catch (e) {
            return new Failure(e);
        }
    }

    toString() {
        return `Exception.Success(${this._value})`;
    }
}

class Failure extends Exception {
    toString() {
        return `Exception.Failure(${this._value.message})`;
    }
}

// Пример использования
const success = Exception.of('Hello World').map(s => s.toUpperCase());
console.log(success.toString()); // Exception.Success(HELLO WORLD)

const failure = Exception.of('Hello World').map(s => { throw new Error('Oops!'); });
console.log(failure.toString()); // Exception.Failure(Oops!)

В этом примере Exception - это монада, которая может содержать успешное значение (Success) или ошибку (Failure). Метод map позволяет применить функцию к содержимому монады, если оно есть и не является ошибкой. Если монада содержит ошибку (Failure), то map просто возвращает монаду с ошибкой.

# Монада List

Монада List в JavaScript может быть использована для работы с списками в функциональном стиле. Вот пример реализации и использования монады List:
В этом примере List - это монада, которая содержит список значений. Метод map позволяет применить функцию к каждому элементу списка.

class List {
    constructor(values) {
        this._values = values;
    }

    static of(values) {
        return new List(values);
    }

    map(fn) {
        return List.of(this._values.map(fn));
    }

    join() {
        return List.of(this._values.reduce((acc, list) => acc.concat(list._values), []));
    }

    toString() {
        return `List(${this._values.join(', ')})`;
    }
}

// Пример использования
const list = List.of([1, 2, 3, 4, 5]).map(x => x * 2);
console.log(list.toString()); // List(2, 4, 6, 8, 10)

const nestedList = List.of([List.of([1, 2]), List.of([3, 4])]).join();
console.log(nestedList.toString()); // List(1, 2, 3, 4)

В этом примере List - это монада, которая содержит список значений. Метод map позволяет применить функцию к каждому элементу списка. Метод join используется для "сглаживания" вложенных списков.

# Монада State

GitHub Copilot
Монада State в JavaScript используется для управления состоянием в функциональном стиле. Вот пример реализации и использования монады State:

class State {
    constructor(runState) {
        this.runState = runState;
    }

    static of(a) {
        return new State(s => [a, s]);
    }

    map(fn) {
        return new State(s => {
            const [a, newState] = this.runState(s);
            return [fn(a), newState];
        });
    }

    chain(fn) {
        return new State(s => {
            const [a, newState] = this.runState(s);
            const state = fn(a);
            return state.runState(newState);
        });
    }
}

// Пример использования
const increment = new State(s => [s, s + 1]);
const double = x => State.of(x * 2);

const stateMonad = increment.chain(double);

const [result, finalState] = stateMonad.runState(2);
console.log(result); // 4
console.log(finalState); // 3

В этом примере State - это монада, которая содержит функцию, принимающую текущее состояние и возвращающую пару [результат, новое состояние]. Метод map позволяет применить функцию к результату, а метод chain позволяет применить функцию, которая возвращает новую монаду State.

# Монада Reader

Монада Reader в JavaScript используется для передачи общего окружения или конфигурации. Вот пример реализации и использования монады Reader

class Reader {
    constructor(run) {
        this.run = run;
    }

    static of(a) {
        return new Reader(() => a);
    }

    map(fn) {
        return new Reader(r => fn(this.run(r)));
    }

    chain(fn) {
        return new Reader(r => fn(this.run(r)).run(r));
    }
}

// Пример использования
const addConfigValue = new Reader(config => config.value + 10);
const multiplyConfigValue = x => Reader.of(x * 2);

const readerMonad = addConfigValue.chain(multiplyConfigValue);

const result = readerMonad.run({ value: 5 });
console.log(result); // 30

В этом примере Reader - это монада, которая содержит функцию, принимающую некоторое окружение и возвращающую результат. Метод map позволяет применить функцию к результату, а метод chain позволяет применить функцию, которая возвращает новую монаду Reader.

# Монада Writer

Монада Writer в JavaScript используется для логирования или сохранения истории вычислений вместе с результатом. Вот пример реализации и использования монады Writer:

class Writer {
    constructor(value, log = '') {
        this.value = value;
        this.log = log;
    }

    static of(value, log = '') {
        return new Writer(value, log);
    }

    map(fn) {
        return Writer.of(fn(this.value), this.log);
    }

    chain(fn) {
        const writer = fn(this.value);
        return Writer.of(writer.value, this.log + writer.log);
    }

    toString() {
        return `Writer(${this.value}, ${this.log})`;
    }
}

// Пример использования
const addFive = x => Writer.of(x + 5, 'Added 5. ');
const multiplyByTwo = x => Writer.of(x * 2, 'Multiplied by 2. ');

const writerMonad = Writer.of(5).chain(addFive).chain(multiplyByTwo);

console.log(writerMonad.toString()); // Writer(20, Added 5. Multiplied by 2.)

В этом примере Writer - это монада, которая содержит значение и лог. Метод map позволяет применить функцию к значению, а метод chain позволяет применить функцию, которая возвращает новую монаду Writer, и объединяет логи.

# Монада Continuation

Монада Continuation в JavaScript используется для управления асинхронными операциями и обработки вложенных вызовов. Вот пример реализации и использования монады Continuation:

class Continuation {
    constructor(run) {
        this.run = run;
    }

    static of(a) {
        return new Continuation(k => k(a));
    }

    map(fn) {
        return new Continuation(k => this.run(a => k(fn(a))));
    }

    chain(fn) {
        return new Continuation(k => this.run(a => fn(a).run(k)));
    }
}

// Пример использования
const addFive = x => Continuation.of(x + 5);
const multiplyByTwo = x => Continuation.of(x * 2);

const continuationMonad = Continuation.of(5).chain(addFive).chain(multiplyByTwo);

continuationMonad.run(result => console.log(result)); // 20

В этом примере Continuation - это монада, которая содержит функцию, принимающую функцию-продолжение и вызывающую её с результатом. Метод map позволяет применить функцию к результату, а метод chain позволяет применить функцию, которая возвращает новую монаду Continuation.

# Монада Async

Монада Async в JavaScript используется для управления асинхронными операциями. Вот пример реализации и использования монады Async:

class Async {
    constructor(exec) {
        this.exec = exec;
    }

    static of(value) {
        return new Async(resolve => resolve(value));
    }

    map(fn) {
        return new Async(resolve => this.exec(value => resolve(fn(value))));
    }

    chain(fn) {
        return new Async(resolve => this.exec(value => fn(value).exec(resolve)));
    }
}

// Пример использования
const delay = (ms, value) => new Async(resolve => setTimeout(() => resolve(value), ms));

const asyncMonad = Async.of('Hello').chain(value => delay(1000, value + ' World'));

asyncMonad.exec(console.log); // Выводит "Hello World" через 1 секунду

В этом примере Async - это монада, которая содержит функцию, принимающую функцию-резолвер и вызывающую её с результатом. Метод map позволяет применить функцию к результату, а метод chain позволяет применить функцию, которая возвращает новую монаду Async.

# Монада Identity

Монада Identity в JavaScript используется для оборачивания значения без добавления дополнительной функциональности. Вот пример реализации и использования монады Identity:

class Identity {
    constructor(value) {
        this.value = value;
    }

    static of(value) {
        return new Identity(value);
    }

    map(fn) {
        return Identity.of(fn(this.value));
    }

    chain(fn) {
        return fn(this.value);
    }
}

// Пример использования
const addFive = x => Identity.of(x + 5);
const multiplyByTwo = x => Identity.of(x * 2);

const identityMonad = Identity.of(5).chain(addFive).chain(multiplyByTwo);

console.log(identityMonad.value); // 20

В этом примере Identity - это монада, которая просто оборачивает значение. Метод map позволяет применить функцию к обернутому значению, а метод chain позволяет применить функцию, которая возвращает новую монаду Identity.

# Монада Either

Монада Either в JavaScript также используется для обработки ошибок, предоставляя два возможных результата: Left (ошибка) или Right (успех). Вот пример реализации и использования монады Either:

class Either {
    constructor(left, right) {
        this.left = left;
        this.right = right;
    }

    static Left(value) {
        return new Either(value, null);
    }

    static Right(value) {
        return new Either(null, value);
    }

    map(fn) {
        if (this.right) {
            return Either.Right(fn(this.right));
        } else {
            return this;
        }
    }

    chain(fn) {
        if (this.right) {
            return fn(this.right);
        } else {
            return this;
        }
    }
}

// Пример использования
const divide = (numerator, denominator) => {
    if (denominator === 0) {
        return Either.Left("Cannot divide by zero");
    } else {
        return Either.Right(numerator / denominator);
    }
};

const addOne = x => Either.Right(x + 1);

const eitherMonad = divide(10, 0).chain(addOne);

if (eitherMonad.right) {
    console.log(eitherMonad.right);
} else {
    console.log(eitherMonad.left); // Выводит "Cannot divide by zero"
}

В этом примере Either - это монада, которая содержит либо значение left (ошибка), либо значение right (успех). Методы map и chain применяют функцию к значению right, если оно существует, иначе возвращают текущий объект.

# Монада IO

Монада IO в JavaScript используется для обработки ввода/вывода в чисто функциональном стиле. Вот пример реализации и использования монады IO:

class IO {
    constructor(effect) {
        if (typeof effect !== 'function') {
            throw 'IO Usage: function required';
        }
        this.effect = effect;
    }

    static of(a) {
        return new IO(() => a);
    }

    map(fn) {
        return new IO(() => fn(this.effect()));
    }

    chain(fn) {
        return this.effect().chain(fn);
    }

    run() {
        return this.effect();
    }
}

// Пример использования
const read = (document, id) => new IO(() => document.querySelector(`#${id}`).innerHTML);
const write = (document, id) => (val) => new IO(() => {
    document.querySelector(`#${id}`).innerHTML = val;
});

const changeToHelloWorld = read(document, 'input')
    .map(str => str.replace(/Hello/g, 'Hello, World'))
    .chain(write(document, 'output'));

// Для выполнения эффекта:
changeToHelloWorld.run();

В этом примере IO - это монада, которая содержит функцию, представляющую собой эффект ввода/вывода. Метод map позволяет применить функцию к результату эффекта, а метод chain позволяет применить функцию, которая возвращает новую монаду IO. Метод run выполняет эффект.

Пожалуйста, обратите внимание, что приведенный выше код не будет работать в среде Node.js, так как он использует API браузера (document.querySelector).

# Монада Promise

Монада Promise в JavaScript используется для управления асинхронными операциями. Promise является встроенным типом в JavaScript и обеспечивает монадический интерфейс для работы с асинхронными операциями.

Вот пример использования монады Promise:

const promiseMonad = new Promise((resolve, reject) => {
    setTimeout(() => resolve("Hello, World!"), 1000);
});

promiseMonad
    .then(value => value + " How are you?")
    .then(console.log)
    .catch(console.error);

В этом примере Promise - это монада, которая оборачивает асинхронную операцию. Метод then позволяет применить функцию к результату операции, когда она завершится. Метод catch позволяет обработать возможные ошибки.