# Каррирование

Каррирование - это процесс преобразования функции с несколькими аргументами в функцию с одним аргументом, которая возвращает другую функцию, которая принимает следующий аргумент, и так далее.

В JavaScript, вы можете реализовать каррирование с помощью замыканий. Вот пример функции каррирования:

function curry(fn) {
    return function curried(...args) {
        if (args.length >= fn.length) {
            return fn.apply(this, args);
        } else {
            return function(...args2) {
                return curried.apply(this, args.concat(args2));
            }
        }
    };
}

// Использование
const sum = (a, b, c) => a + b + c;
const curriedSum = curry(sum);

console.log(curriedSum(1)(2)(3)); // 6
console.log(curriedSum(1, 2)(3)); // 6
console.log(curriedSum(1, 2, 3)); // 6

В этом примере, curry принимает функцию fn и возвращает новую функцию curried. Функция curried проверяет, достаточно ли аргументов для вызова fn. Если да, то она вызывает fn с этими аргументами. Если нет, то она возвращает новую функцию, которая ожидает остальные аргументы.

Каррирование полезно для создания специализированных функций из более общих функций и для упрощения композиции функций.

Каррирование функции с двумя аргументами:

const multiply = (a, b) => a * b;
const curriedMultiply = curry(multiply);

console.log(curriedMultiply(2)(3)); // 6

В этом примере, curriedMultiply - это каррированная версия функции multiply. Вы можете вызвать curriedMultiply(2)(3) для умножения 2 на 3.

Каррирование функции для создания специализированных функций:

const greet = (greeting, name) => `${greeting}, ${name}!`;
const curriedGreet = curry(greet);

const sayHello = curriedGreet("Hello");
const sayHi = curriedGreet("Hi");

console.log(sayHello("John")); // "Hello, John!"
console.log(sayHi("Jane")); // "Hi, Jane!"

Здесь curriedGreet - это каррированная версия функции greet. Вы можете использовать curriedGreet для создания новых функций, таких как sayHello и sayHi, которые имеют фиксированное приветствие.

Каррирование функции для работы с массивами:

const map = curry((fn, arr) => arr.map(fn));

const double = x => x * 2;
const doubleAll = map(double);

console.log(doubleAll([1, 2, 3, 4])); // [2, 4, 6, 8]

В этом примере, map - это каррированная версия встроенного метода Array.prototype.map. Вы можете использовать map для создания новых функций, таких как doubleAll, которые применяют определенную функцию ко всем элементам массива.
