# Идентичность

В JavaScript, идентичность относится к концепции сравнения значений. JavaScript предоставляет два оператора для проверки идентичности: === и !==, которые проверяют равенство без приведения типов.

Это означает, что === проверяет равенство значения и типа, в то время как == проверяет только равенство значения, приводя значения к одному типу при необходимости.

console.log(1 === 1); // true
console.log('1' === 1); // false
console.log(1 === '1'); // false
console.log(0 === false); // false
console.log(null === undefined); // false
console.log(NaN === NaN); // false

!== работает аналогично, но проверяет неравенство идентичности.

console.log(1 !== '1'); // true
console.log(1 !== 1); // false
console.log(0 !== false); // true
console.log(null !== undefined); // true
console.log(NaN !== NaN); // true

Это важная концепция в JavaScript, особенно при работе с условными операторами и циклами.