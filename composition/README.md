# Композиция функций

Композиция функций - это процесс соединения двух и более функций для создания новой функции. Результат одной функции передается в качестве аргумента другой функции.

В JavaScript, вы можете создать функцию для композиции функций следующим образом:

function compose(...fns) {
    return fns.reduce((f, g) => (...args) => f(g(...args)));
}

// Использование
const double = x => x * 2;
const increment = x => x + 1;

const doubleThenIncrement = compose(increment, double);

console.log(doubleThenIncrement(3)); // 7

В этом примере, compose принимает список функций и возвращает новую функцию, которая применяет эти функции справа налево. В данном случае, doubleThenIncrement сначала удваивает аргумент, а затем увеличивает его на 1.

Обратите внимание, что порядок функций в compose важен. Если вы поменяете порядок на compose(double, increment), то получите функцию, которая сначала увеличивает аргумент на 1, а затем удваивает его.

Композиция функций для преобразования строк:

const toUpperCase = str => str.toUpperCase();
const exclaim = str => `${str}!`;

const shout = compose(exclaim, toUpperCase);

console.log(shout("hello")); // "HELLO!"

В этом примере, функция shout преобразует строку в верхний регистр, а затем добавляет восклицательный знак в конец.

Композиция функций для работы с массивами:

const double = x => x * 2;
const map = fn => arr => arr.map(fn);

const doubleAll = compose(map(double), Array.of);

console.log(doubleAll(1, 2, 3, 4)); // [2, 4, 6, 8]

Здесь функция doubleAll создает массив из своих аргументов, а затем удваивает каждый элемент массива.

Композиция функций для работы с объектами:

const users = [
    { name: "John", age: 20 },
    { name: "Jane", age: 30 },
    { name: "Bob", age: 40 }
];

const getNames = arr => arr.map(user => user.name);
const sort = arr => arr.sort();

const sortNames = compose(sort, getNames);

console.log(sortNames(users)); // ["Bob", "Jane", "John"]

В этом примере, функция sortNames извлекает имена из массива пользователей и сортирует их в алфавитном порядке.