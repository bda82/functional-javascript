# Функции высокого порядка

Функции высокого порядка (Higher-Order Functions) в JavaScript - это функции, которые могут принимать другие функции в качестве аргументов и/или возвращать другие функции.

Функции, принимающие другие функции в качестве аргументов:

const numbers = [1, 2, 3, 4, 5];

const doubled = numbers.map(function(number) {
    return number * 2;
});

console.log(doubled); // [2, 4, 6, 8, 10]

В этом примере, map - это функция высокого порядка, которая принимает другую функцию в качестве аргумента и применяет ее ко всем элементам массива.

Функции, возвращающие другие функции:

function greaterThan(n) {
    return function(m) {
        return m > n;
    };
}

const greaterThan10 = greaterThan(10);

console.log(greaterThan10(11)); // true

В этом примере, greaterThan - это функция высокого порядка, которая возвращает другую функцию. Возвращаемая функция затем используется для проверки, больше ли заданное число 10.

Функции, принимающие и возвращающие другие функции:

function compose(f, g) {
    return function(x) {
        return f(g(x));
    };
}

const square = x => x * x;
const double = x => x * 2;

const squareOfDouble = compose(square, double);

console.log(squareOfDouble(5)); // 100

В этом примере, compose - это функция высокого порядка, которая принимает две функции в качестве аргументов и возвращает новую функцию. Возвращаемая функция затем используется для вычисления квадрата удвоенного значения.

# Некоторые функции ввсокого порядка в JavaScript

Array.prototype.map(): This method creates a new array with the results of calling a provided function on every element in the array.
console.log(squares); // [1, 4, 9, 16, 25]

const numbers = [1, 2, 3, 4, 5];
const squares = numbers.map(x => x * x);
console.log(squares); // [1, 4, 9, 16, 25]

Array.prototype.filter(): This method creates a new array with all elements that pass the test implemented by the provided function.

const numbers = [1, 2, 3, 4, 5];
const evens = numbers.filter(x => x % 2 === 0);
console.log(evens); // [2, 4]

Array.prototype.reduce(): This method applies a function against an accumulator and each element in the array (from left to right) to reduce it to a single value.

const numbers = [1, 2, 3, 4, 5];
const sum = numbers.reduce((total, x) => total + x, 0);
console.log(sum); // 15

Function.prototype.bind(): This method creates a new function that, when called, has its this keyword set to the provided value, with a given sequence of arguments preceding any provided when the new function is called.

const module = {
    x: 42,
    getX: function() {
        return this.x;
    }
};

const unboundGetX = module.getX;
console.log(unboundGetX()); // undefined

const boundGetX = unboundGetX.bind(module);
console.log(boundGetX()); // 42

setTimeout() and setInterval(): These global functions execute a provided function after a set number of milliseconds. setTimeout executes the function once, while setInterval keeps executing it at the given interval until it is stopped.

setTimeout(() => console.log('Hello after 2 seconds'), 2000);